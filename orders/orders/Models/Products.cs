﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace orders.Models
{
    public class Products
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
        public int BrandId { get; set; }

        public string BrandName { get; set; }

    }
}
