﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace orders.Models
{
    public class Brands
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }
}
