﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using orders.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace orders.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public BrandsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        //get brands
        [HttpGet]

        public JsonResult Get()
        {
            string query = @"select * from getbrand();";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }


        //create product
        [HttpPost]

        public JsonResult Post(Brands b)
        {
            string query = @"CALL createBrand(@brandname)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@brandname", b.BrandName );

                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("successed!");
        }
        //update product
        [HttpPut]

        public JsonResult Update(Brands b)
        {
            string query = @"CALL updateBrand(@brandid,@brandname)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@brandid", b.BrandId);
                    myCommand.Parameters.AddWithValue("@brandname", b.BrandName);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("successed!");
        }

        //delete product
        [HttpDelete]
        [Route("{id}")]

        public JsonResult Delete(Brands b, int id)
        {
            string query = @"CALL deleteBrand(@brandid)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@brandid", id);

                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Deleted!");
        }
    }
}
