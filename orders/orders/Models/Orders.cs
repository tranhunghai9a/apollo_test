﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace orders.Models
{
    public class Orders
    {
        public int OrderId { get; set; }
        public int TotalPrice { get; set; }
    }
}
