﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using orders.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace orders.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public OrderItemsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //get OrderItems
        [HttpGet]

        public JsonResult Get()
        {
            string query = @"select * from public.getorderitem";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        //get OrderItems by orderid
        [HttpGet]
        [Route("{id}")]

        public JsonResult GetById(int id)
        {
            string query = @"select * from getOrderItemById("+id+");";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }


        //create OrderItem
        [HttpPost]

        public JsonResult Post(OrderItems p)
        {
            string query = @"CALL createOrderItem(@OrderId, @ProductId, @Quantity, @Price)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@OrderId", p.OrderId);
                    myCommand.Parameters.AddWithValue("@ProductId", p.ProductId);
                    myCommand.Parameters.AddWithValue("@quantity", p.Quantity);
                    myCommand.Parameters.AddWithValue("@price", p.ItemPrice);

                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("successed!");
        }
        //update orderItem
        [HttpPut]

        public JsonResult Update(OrderItems p)
        {
            string query = @"CALL updateOrderItem(@itemid, @OrderId, @ProductId, @Quantity, @ItemPrice)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@itemId", p.ItemId);
                    myCommand.Parameters.AddWithValue("@OrderId", p.OrderId);
                    myCommand.Parameters.AddWithValue("@ProductId", p.ProductId);
                    myCommand.Parameters.AddWithValue("@Quantity", p.Quantity);
                    myCommand.Parameters.AddWithValue("@ItemPrice", p.ItemPrice);

                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("successed!");
        }

        //delete product
        [HttpDelete]
        [Route("{id}")]

        public JsonResult Delete (int id)
        {
            string query = @"CALL deleteOrderItem(@Itemid)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("OrderAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@Itemid", id);

                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Deleted!");
        }
    }
}
