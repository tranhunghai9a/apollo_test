﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace orders.Models
{
    public class OrderItems
    {
        public int OrderId { get; set; }
        public int ItemId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }
        public int ItemPrice { get; set; }
        public string ProductName { get; set; }
    }
}
