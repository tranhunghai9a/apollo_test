create table product(
ProductId serial PRIMARY KEY,
ProductName varchar(500),
Price int,
brandId int,
CONSTRAINT fk_brand
   FOREIGN KEY(BrandId) 
      REFERENCES brand(BrandId)
);

create table brand(
BrandId serial PRIMARY KEY,
BrandName varchar(500)
);

insert into brand(BrandName) values ('Samsung');
insert into brand(BrandName) values ('Apple');
insert into brand(BrandName) values ('Oppo');

select * from brand

insert into product(ProductName,Price,BrandId) values ('Samsung s22 Ultra',20000000,1);
insert into product(ProductName,Price,BrandId) values ('Iphone 13 Pro Max',30000000,2);
insert into product(ProductName,Price,BrandId) values ('Oppo something',15000000,3);

select * from product

create table orders(
OrderId int PRIMARY KEY,
totalPrice int
);




create table orderItems (
ItemId serial,
OrderId int,
ProductId int,
Quantity int,
ItemPrice int,
PRIMARY KEY(ItemId, OrderId),
CONSTRAINT fk_product
   FOREIGN KEY(ProductId) 
      REFERENCES product(ProductId)
);
select * from orderItems



insert into orderItems(OrderId, ProductId, Quantity,ItemPrice)
values(1,1,2,20000000);
insert into orderItems(OrderId, ProductId, Quantity,ItemPrice)
values(1,2,1,30000000);
insert into orderItems(OrderId, ProductId, Quantity,ItemPrice)
values(2,3,3,15000000);

select * from orderItems

Create function getProduct()  
returns table(productId int, productName varchar, brandId int, Price int, brandName varchar)  
language plpgsql  
as  
$$ 
Begin  
   return query select  pr.productId, pr.productName, pr.brandId, pr.price, b.brandName
   from product as pr
   inner join brand as b
   on pr.brandId = b.brandId;
End;  
$$;      

select * from getProduct()

        
CREATE OR REPLACE PROCEDURE createProduct  
(  
	ProductName varchar(500),
	Price int,
	BrandId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   INSERT INTO Product (ProductName,Price,BrandId) VALUES   
    (ProductName, Price,BrandId
    );
END  
$$;  

CALL createProduct('Iphone 10',12000000, 2)



CREATE OR REPLACE PROCEDURE updateProduct  
(  
    UdpProductId int,
	UdpProductName varchar(500),
	UdpPrice int,
	UdpBrandId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   UPDATE product SET
   ProductName = UdpProductName,
   Price = UdpPrice,
   BrandId = UdpBrandId
   where UdpProductId = ProductId;
END  
$$;  

Call updateProduct(2,'Iphone 14 Promax', 32000000,2)


CREATE OR REPLACE PROCEDURE deleteProduct  
(  
    dltProductId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   DELETE FROM product 
   where dltProductId = ProductId;
END  
$$; 
select * from getProduct()
call deleteProduct(4)



//brand api

Create function getBrand()  
returns table(brandId int, brandName varchar)  
language plpgsql  
as  
$$ 
Begin  
   return query select * from brand;
End;  
$$;      
select * from getBrand()
        
CREATE OR REPLACE PROCEDURE createbrand  
(  
	BrandName varchar(500)
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   INSERT INTO Brand (BrandName) VALUES   
    (BrandName
    );
END  
$$;  

CALL createBrand('LG')

select * from brand


CREATE OR REPLACE PROCEDURE updateBrand
(  
    UdpBrandId int,
	UdpBrandName varchar(500)
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   UPDATE brand SET
   BrandName = UdpBrandName
   where UdpBrandId = BrandId;
END  
$$;  

Call updateBrand(4,'HTC')


CREATE OR REPLACE PROCEDURE deleteBrand  
(  
    dltBrandId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   DELETE FROM brand
   where dltBrandId = BrandId;
END  
$$; 

call deleteBrand(4)

/*orders api */

Create function getOrder()  
returns table(orderId int, totalPrice int)  
language plpgsql  
as  
$$ 
Begin  
   return query select o.orderId, o.totalprice from orders as o;
End;  
$$;      
select * from getOrder()

CREATE OR REPLACE FUNCTION GetTotalPrice(id int)  
RETURNS int 
LANGUAGE SQL   
AS   
$$  
    select sum(product.productprice) as totalprice
	from bill
	inner join product
	on bill.billid = product.billid
	where bill.billid = id

$$;




insert into Orders(orderId, totalprice)   values (2,null)     
CREATE OR REPLACE PROCEDURE createOrder
(  
    OrderId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   INSERT INTO Orders(orderid) VALUES(orderid);
END  
$$;  

CALL createOrder(5)






CREATE OR REPLACE PROCEDURE deleteOrder 
(  
    dltOrderId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   DELETE FROM orders
   where dltOrderId = orderId;
END  
$$; 

call deleteOrder(5)



//orderItem api

Create function getOrderItem()  
returns table(itemId int, orderId int, productId int, quantity int, itemPrice int, productName varchar)  
language plpgsql  
as  
$$ 
Begin  
   return query select  i.itemId, i.orderId, i.productId, i.quantity, i.itemPrice, o.productName
   from orderItems as i
   inner join product as o
   on i.productId = o.productId
   ;
End;  
$$;      
select * from getOrderItem()

create view getOrderitem as 
select t.itemid, t.orderid, t.productid, t.quantity, t.quantity * p.price itemprice, p.productName
from orderitems t
inner join product p on p.productid = t.productid

select * from public.getorderitem


Create function getOrderItemById(id int)  
returns table(itemId int, orderId int, productId int, quantity int, itemPrice int, productName varchar)  
language plpgsql  
as  
$$ 
Begin  
   return query select  i.itemId, i.orderId, i.productId, i.quantity, i.itemPrice, o.productName
   from orderItems as i
   inner join product as o
   on i.productId = o.productId
   where i.orderId = id
   ;
End;  
$$;      
select * from getOrderItemById(1)


        
CREATE OR REPLACE PROCEDURE createOrderItem 
(  
	Orderid int,
    ProductId int,
    Quantity int,
    ItemPrice int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   INSERT INTO OrderItems (OrderId, ProductId, Quantity, ItemPrice) VALUES   
    (OrderId, ProductId, Quantity, ItemPrice
    );
END  
$$;  

CALL createOrderItem(2,3,5,20000000)

select * from orderitems


CREATE OR REPLACE PROCEDURE updateOrderItem
(  
    UdpItemId int,
	UdpOrderId int,
    UdpProductId int,
    UdpQuantity int,
    UdpItemPrice int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   UPDATE OrderItems SET
	OrderId = UdpOrderId,
    ProductId = UdpProductId,
    Quantity = UdpQuantity,
    ItemPrice = UdpItemPrice
   where UdpItemId = ItemId;
END  
$$;  

Call updateOrderItem(4,2,1,4,15000000)


CREATE OR REPLACE PROCEDURE deleteOrderItem 
(  
    dltItemId int
)  
LANGUAGE plpgsql AS  
$$  
BEGIN         
   DELETE FROM OrderItems
   where dltItemId = ItemId;
END  
$$; 

call deleteOrderItem(4)


CREATE OR REPLACE FUNCTION GetTotalPrice(id int)  
RETURNS int 
LANGUAGE SQL   
AS   
$$  
    select sum(getorderitem.itemprice) as totalprice
	from getorderitem
    inner join orders
	on getorderitem.orderid = orders.orderid 
    where orders.orderid = id

$$;

select * from orders
select * from public.getorderitem


CREATE OR REPLACE FUNCTION UpdateTotalPrice()
RETURNS Trigger
LANGUAGE plpgsql AS  
$$  
BEGIN         
	UPDATE orders
	SET totalprice = GetTotalPrice(orderid);
	RETURN NULL;
END;

$$

CREATE TRIGGER auto_update
AFTER INSERT OR UPDATE OR DELETE ON product 
    FOR EACH ROW EXECUTE FUNCTION UpdateTotalPrice()
    
CREATE TRIGGER auto_update
AFTER INSERT OR UPDATE OR DELETE ON orderitems
    FOR EACH ROW EXECUTE FUNCTION UpdateTotalPrice()


